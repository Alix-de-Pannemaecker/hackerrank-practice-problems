def print_formatted(number):
    for i in range(1, number + 1, 1):
        print(str(i), "", format(i, '0'), "", "%X" % i, "", "{0:b}".format(i))

print(print_formatted(17))
