def formatting(string):
    text1 = string.split()
    text2 = string.strip(" ")
    text3 = " ".join(text1)
    return text1, text2, text3

test = "  Hello    World!  "
print(formatting(test))


matrix = [[1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12]]

print("Matrix =", matrix)

matrix = [[column for column in range(4)] for row in range(4)]
print(matrix)
