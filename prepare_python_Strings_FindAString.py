def count_substring(string, sub_string):
    if sub_string not in string:
        return 0

    count = 0
    i = 0
    j = 0

    while i < len(string):
        if string[i] == sub_string[0] and len(string[i:len(string)]) <= len(string):
            while j < len(sub_string):
                if string[i + j] == sub_string[j]:
                    print(string[i + j])
                    j += 1
            if j == len(sub_string):
                count += 1
                j = 0
                i += j
        i += 1

    return count

if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()

    count = count_substring(string, sub_string)
    print(count)
